<?php

namespace Drupal\custom_views_styles;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a custom views styles entity type.
 */
interface CustomViewsStylesInterface extends ConfigEntityInterface {

}
