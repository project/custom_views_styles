<?php

namespace Drupal\custom_views_styles\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\custom_views_styles\CustomViewsStylesInterface;

/**
 * Defines the custom views styles entity type.
 *
 * @ConfigEntityType(
 *   id = "custom_views_styles",
 *   label = @Translation("Custom Views Styles"),
 *   label_collection = @Translation("Custom Views Styless"),
 *   label_singular = @Translation("custom views styles"),
 *   label_plural = @Translation("custom views styless"),
 *   label_count = @PluralTranslation(
 *     singular = "@count custom views styles",
 *     plural = "@count custom views styless",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\custom_views_styles\CustomViewsStylesListBuilder",
 *     "form" = {
 *       "add" = "Drupal\custom_views_styles\Form\CustomViewsStylesForm",
 *       "edit" = "Drupal\custom_views_styles\Form\CustomViewsStylesForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "custom_views_styles",
 *   admin_permission = "administer custom_views_styles",
 *   links = {
 *     "collection" = "/admin/structure/custom-views-styles",
 *     "add-form" = "/admin/structure/custom-views-styles/add",
 *     "edit-form" = "/admin/structure/custom-views-styles/{custom_views_styles}",
 *     "delete-form" = "/admin/structure/custom-views-styles/{custom_views_styles}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description"
 *   }
 * )
 */
class CustomViewsStyles extends ConfigEntityBase implements CustomViewsStylesInterface {

  /**
   * The custom views styles ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The custom views styles label.
   *
   * @var string
   */
  protected $label;

  /**
   * The custom views styles status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The custom_views_styles description.
   *
   * @var string
   */
  protected $description;

}
