<?php

namespace Drupal\custom_views_styles\Plugin\views\style;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * The default style plugin for summaries.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "custom_views_styles",
 *   title = @Translation("Custom"),
 *   help = @Translation("Displays the summary unformatted, with option for one after another or inline."),
 *   theme = "views_view_unformatted",
 *   deriver = "Drupal\custom_views_styles\Plugin\Derivative\CustomViewsStyles",
 * )
 */
class CustomViewsStyles extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;


  /**
   * @return array
   */
  public function themeFunctions() {
    $original = parent::themeFunctions();
    $custom_views_styles_id = 'views_view_styles_custom' . $this->getDerivativeId();
    $additional = $this->view->buildThemeFunctions($custom_views_styles_id);
    return array_merge($original, $additional);
  }
}
