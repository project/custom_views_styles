<?php

namespace Drupal\custom_views_styles\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves custom views styles plugin definitions for all custom views styles.
 */
class CustomViewsStyles extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The custom views styles storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $customViewsStylesStorage;

  /**
   * Constructs a CustomViewsStyles object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $custom_views_styles_storage
   *   The custom views styles storage.
   */
  public function __construct(EntityStorageInterface $custom_views_styles_storage) {
    $this->customViewsStylesStorage = $custom_views_styles_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('custom_views_styles')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $custom_views_styles_contents = $this->customViewsStylesStorage->loadMultiple();
    // Reset the discovered definitions.
    $this->derivatives = [];
    /** @var \Drupal\custom_views_styles\Entity\CustomViewsStyles $custom_views_styles_contents */
    foreach ($custom_views_styles_contents as $custom_views_styles_content) {
      $this->derivatives[$custom_views_styles_content->id()] = [
          'title' => $custom_views_styles_content->label(),
        ] + $base_plugin_definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
